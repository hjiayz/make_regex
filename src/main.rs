use make_regex::buildecma262::Options;
use make_regex::buildecma262::build;


fn main() {
    let mut args = std::env::args();
    args.next();
    let s = args.next().unwrap_or_default();
    let opt = args.next().unwrap_or_default();
    let opt = Options::from(opt.as_str());
    println!("{}",build(&s,opt));
}
