
// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use regex_syntax::{hir::{Hir,HirKind, Anchor, Class, ClassBytes, ClassUnicode, ClassUnicodeRange, ClassBytesRange, Literal, WordBoundary, Repetition, RepetitionKind, RepetitionRange, Group, GroupKind}};
use std::panic;

pub struct Config {
    pub named_groups:Vec<String>,
    pub unicode:bool,
}

impl std::fmt::Display for Config {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut s = String::with_capacity(2);
        if self.unicode  {
            s.push_str("u");
        }
        write!(f,"{s}")
    }
}

fn hir_gen(src:&Hir,cfg:&mut Config)->String{
    match src.kind() {
        HirKind::Alternation(alt)=>alt_gen(alt,cfg),
        HirKind::Anchor(anchor)=>anchor_gen(anchor),
        HirKind::Class(cla)=>cla_gen(cla,cfg),
        HirKind::Empty => format!("(?:)"),
        HirKind::Literal(lit) => lit_gen(lit,cfg),
        HirKind::WordBoundary(wb) => wb_gen(wb),
        HirKind::Repetition(rep) => rep_gen(rep,cfg),
        HirKind::Group(group) => group_gen(group,cfg),
        HirKind::Concat(concat) => concat_gen(concat,cfg),
    }
}

fn alt_gen(alt:&Vec<Hir>,cfg:&mut Config)->String{
    alt.into_iter().map(|hir|hir_gen(hir,cfg)).collect::<Vec<String>>().join("|")
}

fn anchor_gen(anchor:&Anchor)->String{
    match anchor {
        Anchor::EndLine=>format!(r"(?=\n|$)"),
        Anchor::EndText=>format!("$"),
        Anchor::StartLine=>format!(r"(?<=^|\n)"),
        Anchor::StartText=>format!("^"),
    }
}

fn arange_gen(cb:&ClassBytesRange,cfg:&mut Config)->String {
    format_range(cb.start() as char,cb.end() as char,cfg)
}

fn ascii_gen(cb:&ClassBytes,cfg:&mut Config)->String{
    let inner=cb.iter().map(|cbr|arange_gen(cbr,cfg)).collect::<Vec<String>>().join("");
    format!("[{}]",inner)
}

fn cla_gen(cla:&Class,cfg:&mut Config)->String{
    match cla {
        Class::Bytes(bytes)=>ascii_gen(bytes,cfg),
        Class::Unicode(unicode)=>unicode_gen(unicode,cfg),
    }
}

fn urange_gen(ur:&ClassUnicodeRange,cfg:&mut Config)->String {
    format_range(ur.start(),ur.end(),cfg)
}

fn unicode_gen(cu:&ClassUnicode,cfg:&mut Config)->String{
    let inner=cu.iter().map(|cur|urange_gen(cur,cfg)).collect::<Vec<String>>().join("");
    format!("[{}]",inner)
}

fn format_range(start:char,end:char,cfg:&mut Config)->String{
    let start = conv_char(start,cfg);
    let end = conv_char(end,cfg);
    if start == end {
        format!("{}",start)
    }
    else {
        format!("{}-{}",start,end)
    }
}

fn conv_char(ch:char,cfg:&mut Config)->String{
    if ch.is_ascii_alphanumeric() {
        return format!("{}",ch);
    }
    if ch.is_ascii() {
        return format!("\\x{:02X}",ch as u32);
    }
    cfg.unicode=true;
    format!("\\u{{{:X}}}",ch as u32)
}

fn lit_gen(lit:&Literal,cfg:&mut Config)->String {
    match lit {
        Literal::Byte(_byte)=>panic!("not utf8"),
        Literal::Unicode(unicode)=>format_range(*unicode, *unicode,cfg)
    }
}

fn wb_gen(wb:&WordBoundary)->String {
    if wb.is_negated(){
        format!("\\B")
    }
    else {
        format!("\\b")
    }
}

fn rep_gen(rep:&Repetition,cfg:&mut Config)->String {
    let q = match rep.kind {
        RepetitionKind::OneOrMore=>"+".to_owned(),
        RepetitionKind::ZeroOrMore=>"*".to_owned(),
        RepetitionKind::ZeroOrOne=>"?".to_owned(),
        RepetitionKind::Range(RepetitionRange::Exactly(n))=>format!("{{{n}}}"),
        RepetitionKind::Range(RepetitionRange::AtLeast(min))=>format!("{{{min},}}"),
        RepetitionKind::Range(RepetitionRange::Bounded(min,max))=>format!("{{{min},{max}}}"),
    };
    let greedy = if rep.greedy {
        ""
    }
    else {
        "?"
    };
    format!("{}{q}{greedy}",hir_gen(&rep.hir,cfg))
}

fn group_gen(group:&Group,cfg:&mut Config)->String{
    match &group.kind{
        GroupKind::CaptureIndex(_id)=>panic!("capturing group name missing."),
        GroupKind::CaptureName { name,.. }=>named_group(&name,&group.hir,cfg),
        GroupKind::NonCapturing => nocapturing_group(&group.hir,cfg),
    }
}

fn named_group(name:&str,hir:&Hir,cfg:&mut Config)->String{
    cfg.named_groups.push(name.to_string());
    format!("(?<{name}>{})",hir_gen(hir,cfg))
}

fn nocapturing_group(hir:&Hir,cfg:&mut Config)->String{
    format!("(?:{})",hir_gen(hir,cfg))
}

fn concat_gen(concat:&Vec<Hir>,cfg:&mut Config)->String{
    concat.into_iter().map(|concat|hir_gen(concat,cfg)).collect::<Vec<String>>().join("")
}

#[derive(Copy,Clone)]
pub struct Options{
    case_insensitive:bool,
    multi_line:bool,
    dot_matches_new_line:bool,
    ignore_whitespace:bool,
    unicode:bool,
    swap_greed:bool,
}

impl<'a> From<&'a str> for Options{
    fn from(src: &'a str) -> Self {
        let mut opts = Options{
            case_insensitive:false,
            multi_line:false,
            dot_matches_new_line:false,
            ignore_whitespace:false,
            unicode:false,
            swap_greed:false,
        };
        for ch in src.chars() {
            match ch {
                'i'=>opts.case_insensitive=true,
                'm'=>opts.multi_line=true,
                's'=>opts.dot_matches_new_line=true,
                'x'=>opts.ignore_whitespace=true,
                'u'=>opts.unicode=true,
                'U'=>opts.swap_greed=true,
                _=>panic!("unknown mode: {}",ch),
            }
        }
        opts
    }
}

pub fn build(s:&str,opts:Options)->String {
    let mut builder = regex_syntax::ParserBuilder::new();
    builder.case_insensitive(opts.case_insensitive);
    builder.multi_line(opts.multi_line);
    builder.dot_matches_new_line(opts.dot_matches_new_line);
    builder.ignore_whitespace(opts.ignore_whitespace);
    builder.unicode(opts.unicode);
    builder.swap_greed(opts.swap_greed);
    let mut parser = builder.build();
    let hir = parser.parse(s).unwrap();
    let mut cfg = Config { unicode: false,named_groups:vec![] };
    let regex = hir_gen(&hir,&mut cfg);
    format!("/{regex}/{cfg}")
}
